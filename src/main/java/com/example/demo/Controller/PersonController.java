package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Entity.Person;
import com.example.demo.Entity.PersonMethoden;
import com.example.demo.Repository.PersonRepository;

@RestController
public class PersonController {

	@Autowired
	PersonRepository personRepository;
	
	@GetMapping("/savePerson")//POSTMapping eig
	String savePerson() {
		Person person = new Person("Burak", 24, "burak.dgn@outlook.de");
		personRepository.save(person);
		PersonMethoden p = new PersonMethoden();
		personRepository.save(p.abfrage());
		return "saved Person";
	}
	
	@GetMapping("/getPerson")
	List<Person> getPerson() {
	List<Person> persons = new ArrayList<Person>();
	personRepository.findAll().forEach(person -> persons.add(person));
	return persons;
	}
	
	@GetMapping("/getPerson/{id}")
	 private Person getPersonbyID(@PathVariable("id") int id) {
        return personRepository.findById(id).get();
    }
	
	@PutMapping("getPerson/{id}")
	private Person updatePerson(@PathVariable("id") int id,
								@RequestParam String name,					
								@RequestParam int age,
								@RequestParam String email)
	{
		Person p = personRepository.findById(id).get();
		p.setName(name);
		p.setAge(age);
		p.setEmailId(email);
		personRepository.save(p);
		return null;
	}
															
	
	@DeleteMapping("/getPerson/{id}")
	private String deletePerson(@PathVariable("id") int id) {
	
		 personRepository.deleteById(id);	
		 return "deleted Person with id " + id;
	}
	
	@DeleteMapping("/delete")
	private String deletePersons() {
	
		 personRepository.deleteAll();
		 return "Deleted all from Table";
	}
}

//Anfrage für PUT sieht wie folgt aus 
//localhost:8080/getPerson/id?name=&age=&email=
//BSP: localhost:8080/getPerson/6?name=Kek&age=27&email=kek
//ändere namen.alter und email der Person mit der id 6 im Bsp 